package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        int size = inputNumbers.size();
        if (Math.sqrt(8*size+1) % (0.1) % 10 != 0) { // Check the full square
            throw new CannotBuildPyramidException();
        }
        Collections.sort(inputNumbers);
        int numberLines = (int) (Math.sqrt(8*size+1)-1)/2;
        int[] [] result = new int [numberLines][numberLines + numberLines - 1];
        for (int i = 0; i < numberLines; ++i ) {
            int zeros = numberLines - i - 1;
            int k = 0;
            while (k != zeros) {
                result[i][k++] = result[i][numberLines + numberLines - 2 - k] = 0; // Right associative
            }
            int begin = (int) ((i+1)*(i))/2; // Triangle recurrent
            int end = (int) ((i+2)*(i+1))/2; // Triangle recurrent
            while(begin != end) {
                result[i][k++] = inputNumbers.get(begin++);
                if ( k == numberLines + numberLines - 1) {
                    continue;
                }
                result[i][k++] = 0;
            }
        }
        return result;
    }

}
