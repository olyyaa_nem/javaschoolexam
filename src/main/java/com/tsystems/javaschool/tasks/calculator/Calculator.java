package com.tsystems.javaschool.tasks.calculator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if ((statement == null) || statement.equals("") || statement.contains(",")
                || statement.contains("//") || statement.contains("**") || statement.contains("++") || statement.contains("--")) {
            return null;
        }
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");
        Object result;
        try {
            result = engine.eval(statement);
        } catch (ScriptException e) {
            return null;
        }
        return result.toString().equals("Infinity") ? null : result.toString();
    }
}